<?php
class autoloader {

  public static function load($name) {
    $pfad= __DIR__ . DIRECTORY_SEPARATOR . $name . '.php';
  
    if (file_exists($pfad)) 
    {
      require_once $pfad;
    } 
    else 
    {
      return false; 
    }
  }
}
?>