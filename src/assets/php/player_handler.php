<?php
    ini_set('include_path','../assets');
    require_once("SteamCondenser.php");
    require_once("autoloader.php");

    spl_autoload_register(array('autoloader', 'load'));

    $playerid = $_POST['playeridpass'];
    $playerfield = $_POST['playerfieldpass'];

    if ($playerid == null) {
        $playername = "No Player selected";
    }

    else {
        $steamuser = SteamCondenser/Community/SteamId::create($playerid);
        $playername = $steamuser->getNickname();
        echo "Welcome, " . $playername;
    }
        
    $players = simplexml_load_file("/assets/players.xml");
    
    $playerfieldid = $playerfield . '_name';
    $players->$playerfieldid = $playername;

    $playerfieldid = $playerfield . '_id';
    $players->$playerfieldid = $playerid;
    
    $players->asXML('assets/players.xml');
?>