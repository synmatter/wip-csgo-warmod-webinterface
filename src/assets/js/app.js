$(document).foundation();

var playerfield = 'test';
function addplayer(element, event) {
    playerfield = element.parentNode.id;
    var playerteam = playerfield.match(['a-z']);
    if (playerfield == "c")
        document.getElementById("playerteam").innerHTML = "to Counter Terrorist team";
    else if (playerfield == "t")
        document.getElementById("playerteam").innerHTML = "to Terrorist team";
}

function submitplayer(form, event) {
    event.preventDefault();
    var submitplayer = form.playerurl.value;
    var n = submitplayer.lastIndexOf("/");
    var playerid = submitplayer.substring(n + 1);
    $.post('assets/php/player_handler.php', {playerfieldpass: playerfield, playeridpass: playerid});
    $('#add_player').foundation('reveal', 'close');
}

function removeplayer(element, event) {
    playerfield = element.parentNode.id;
    $.post('assets/php/player_handler.php', {playerfieldpass: playerfield, playeridpass: null});
}

//Auto page update
function getXmlHttpRequestObject() {
	 if (window.XMLHttpRequest) {
		 return new XMLHttpRequest();
	 } else if (window.ActiveXObject) {
		 return new ActiveXObject("Microsoft.XMLHTTP");
	 } else {
		 alert("Your Browser does not support AJAX!\nIt's about time to upgrade don't you think?");
	 }
 }
 
//XmlHttpRequest object
 var req = getXmlHttpRequestObject(); 

function getRequest(resource) {
 	
	// handle the case where a querystring is not detected
	var char = "&";
 	if(resource.indexOf("?", 0) == -1) {
		char = "?";
	}
		
	 if (req.readyState == 4 || req.readyState == 0) {
		 req.open("GET", resource + char + 'ms=' + new Date().getTime(), true);
		 req.onreadystatechange = handleResponse;
		 req.send(null);
		 return false;
	 }
 }

function handleResponse() {
		 if (req.readyState == 4) 
		 {
			parseState(req.responseXML);
		 }
	} 

	function parseState(xDoc){
		if(xDoc == null)
			return
			
		//Reference the <div> tag with the ID "text"; <div id="text">XML</div>	
		var target = document.getElementById("cter_1_field");
		//Get the value of the xml node named <text> and place the value in <div id="text">here</div>
		target.innerHTML = xDoc.getElementsByTagName("cter_1_name")[0].childNodes[0].nodeValue;
		
		target = document.getElementById("cter_2_field");
		target.innerHTML = xDoc.getElementsByTagName("cter_2_name")[0].childNodes[0].nodeValue;
		
		target = document.getElementById("cter_3_field");
		target.innerHTML = xDoc.getElementsByTagName("cter_3_name")[0].childNodes[0].nodeValue;
		
		target = document.getElementById("cter_4_field");
		target.innerHTML = xDoc.getElementsByTagName("cter_4_name")[0].childNodes[0].nodeValue;
        
        target = document.getElementById("cter_5_field");
		target.innerHTML = xDoc.getElementsByTagName("cter_5_name")[0].childNodes[0].nodeValue;
        
        target = document.getElementById("ter_1_field");
		target.innerHTML = xDoc.getElementsByTagName("ter_1_name")[0].childNodes[0].nodeValue;
        
        target = document.getElementById("ter_2_field");
		target.innerHTML = xDoc.getElementsByTagName("ter_2_name")[0].childNodes[0].nodeValue;
        
        target = document.getElementById("ter_3_field");
		target.innerHTML = xDoc.getElementsByTagName("ter_3_name")[0].childNodes[0].nodeValue;
        
        target = document.getElementById("ter_4_field");
		target.innerHTML = xDoc.getElementsByTagName("ter_4_name")[0].childNodes[0].nodeValue;
        
        target = document.getElementById("ter_5_field");
		target.innerHTML = xDoc.getElementsByTagName("ter_5_name")[0].childNodes[0].nodeValue;
	}

setInterval(getRequest('assets/players.xml'), 1);